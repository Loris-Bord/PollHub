const buttonls = document.getElementById("light");
let currentModeLight = true;
let lien_css = document.createElement('link');

lien_css.rel = "stylesheet";
lien_css.type = "text/css";

document.getElementsByTagName("head").item(0).appendChild(lien_css);

function setBackGroundMode(){
    localStorage.clear();
    if(currentModeLight){
        lien_css.href = "css/styles.css";
        buttonls.innerText = "NUIT";
        currentModeLight = false;
    }else{
        lien_css.href = "css/stylesClair.css";
        buttonls.innerText = "JOUR";
        currentModeLight = true;
    }
    localStorage.setItem("mode", currentModeLight);
}



buttonls.addEventListener("click", setBackGroundMode);

currentModeLight = localStorage.getItem("mode") !== 'true';
setBackGroundMode();
