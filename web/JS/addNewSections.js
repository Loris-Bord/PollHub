
function ajouter(id)
{
    let number = 0;

    if (id < 2){
        number = document.querySelectorAll('#sections_' + id + ' > div').length + 1;
    } else {
        number = document.querySelectorAll('#sections_' + id + ' > div').length;
    }

    let div = document.createElement('div');
    let br = document.createElement('br');
    let titreLabel = document.createElement('label');
    let titreTextarea = document.createElement('textarea');

    let descriptionLabel = document.createElement('label');
    let descriptionTextarea = document.createElement('textarea');

    titreTextarea.setAttribute('name', 'titreSection' + number);
    titreTextarea.setAttribute('id', 'titreSection' + number + '_id');
    titreTextarea.setAttribute('rows', '1');
    titreTextarea.setAttribute('cols', '50');
    titreTextarea.setAttribute("class", "form-control");
    titreTextarea.setAttribute('style', 'font-weight:bold;');
    titreLabel.innerHTML = "Titre section " + number;
    titreLabel.setAttribute('for', 'titreSection' + number + '_id');
    titreLabel.setAttribute('class', "form-label");


    descriptionTextarea.setAttribute('name', 'descriptionSection' + number);
    descriptionTextarea.setAttribute('id', 'descriptionSection' + number + '_id');
    descriptionTextarea.setAttribute('rows', '5');
    descriptionTextarea.setAttribute('cols', '75');
    descriptionTextarea.setAttribute("class", "form-control");
    descriptionLabel.innerHTML = "Description section " + number;
    descriptionLabel.setAttribute('for', 'descriptionSection' + number + '_id');
    descriptionLabel.setAttribute('class', 'my-2');
    titreLabel.setAttribute('class', "form-label");

    div.appendChild(titreLabel);
    div.appendChild(titreTextarea);
    div.appendChild(descriptionLabel);
    div.appendChild(descriptionTextarea);
    div.appendChild(br);
    div.appendChild(br);



    document.getElementById("sections_" + id).appendChild(div);
    var simple = new SimpleMDE({ element: document.getElementById("descriptionSection"+ number+"_id"),
        toolbar : ["bold", "italic", "heading", "|", "quote", "strikethrough", "unordered-list", "ordered-list", "|", "link", "table", "image", "|", "preview", "guide"]});

    form.addEventListener('submit', function(event) {
        const markdownValue = simple.value();

        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = 'descriptionSection'+ number +'Md';
        hiddenField.value = markdownValue;
        hiddenField.required = true;
        form.appendChild(hiddenField);
    });
}

function supprimer(id)
{
    let nb;

    if (id < 2){
        nb = document.querySelectorAll('#sections_' + id +' > div').length;
    } else {
        nb = document.querySelectorAll('#sections_' + id +' > div').length - 1;
    }

    if (nb > 1){
        document.getElementById("titreSection" + nb + "_id").parentElement.remove();
    }
}


